package com.bloxadmin.http.websocket;

import com.bloxadmin.http.Request;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class Client {

    private Socket socket;
    private Request initReqest;
    private String accept;
    private String key;
    private OutputStream os;
    private InputStream is;
    private Scanner in;
    private PrintStream out;

    public Client(Socket socket, Request initReqest) throws NoSuchAlgorithmException, IOException {
        os = socket.getOutputStream();
        is = socket.getInputStream();
        out = new PrintStream(os);
        in = new Scanner(is, "UTF-8");

        this.socket = socket;
        this.initReqest = initReqest;
        this.key = initReqest.getHeader("Sec-WebSocket-Key");
        System.out.println("Key: " + key);
        this.accept = DatatypeConverter.printBase64Binary(MessageDigest.getInstance("SHA-1").digest(
                (key + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").getBytes("UTF-8")));
        System.out.println("Accept: " + accept);

        byte[] response = ("HTTP/1.1 101 Switching Protocols\r\n"
                + "Connection: Upgrade\r\n"
                + "Upgrade: websocket\r\n"
                + "Sec-WebSocket-Accept: " + accept
                + "\r\n\r\n").getBytes("UTF-8");

        out.write(response, 0, response.length);

        byte[] payload;
        boolean fin = false;
        boolean rsv1 = false;
        boolean rsv2 = false;
        boolean rsv3 = false;
        byte opcode = 0;
        int c = -1;
        byte[] head = new byte[10];
        int length = 0;
        while (socket.isConnected()) {
            if (is.read(head) == 0)
                continue;

            byte b = head[0];
            fin = (getBit(b, 0) != 0);
            rsv1 = (getBit(b, 1) != 0);
            rsv2 = (getBit(b, 2) != 0);
            rsv3 = (getBit(b, 3) != 0);
            opcode = (byte) (b & 0x0F);

            length = head[1] & 0x1F;

            System.out.println("Length: " + ((Object) (head[1])).toString());

            System.out.println("Opcode: " + opcode);
            System.out.println("fin: " + fin);
            System.out.println("rsv1: " + rsv1);
            System.out.println("rsv2: " + rsv2);
            System.out.println("rsv3: " + rsv3);
        }
        System.out.println("End loop");
    }

    private byte getBit(byte b, int p) {
        return (byte) ((b >> p) & 1);
    }

    public void sendMessage(Frame frame) {

    }

    public void onMessage() {

    }

    public void onDisconect() {

    }
}
