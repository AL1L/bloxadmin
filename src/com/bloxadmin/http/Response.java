package com.bloxadmin.http;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Response {

    private Map<String, String> headers = new HashMap<>();
    private StatusCode statusCode = StatusCode.OK;
    private byte[] body = {};

    public Response() {
        addHeader("server", "BloxAdmin/1.0");
        addHeader("date", new Date());
        addHeader("content-type", "text/plain");
        addHeader("content-length", 0);
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public void addHeader(String header, Object value) {
        if (value == null)
            return;
        header = cap(header.toLowerCase());
        headers.put(header, value.toString());
    }

    public void setHeader(String header, Object value) {
        addHeader(header, value);
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public byte[] getBody() {
        return body;
    }

    public String getStringBody() {
        return new String(getBody());
    }

    public void setBody(String body) {
        setBody(body.getBytes());
    }

    public void setBody(byte[] body) {
        this.body = body;
        addHeader("content-length", body.length);
    }

    private static String cap(String str) {
        StringBuffer b = new StringBuffer();
        Pattern p = Pattern.compile("\\b([\\w-]){1}");
        Matcher m = p.matcher(str);
        while (m.find()) {
            String s = m.group(1);
            m.appendReplacement(b, s.toUpperCase());
        }
        m.appendTail(b);
        return b.toString();
    }
}
