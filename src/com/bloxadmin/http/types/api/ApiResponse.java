package com.bloxadmin.http.types.api;

import com.bloxadmin.http.Response;
import com.bloxadmin.http.StatusCode;
import com.google.gson.*;

public class ApiResponse extends Response {

    private static Gson gson = new GsonBuilder().create();
    private static JsonParser jsonParser = new JsonParser();
    private JsonObject json;

    public ApiResponse() {
        json = new JsonObject();
        json.addProperty("host", "");
        json.addProperty("code", StatusCode.OK.getCode());
        json.addProperty("message", StatusCode.OK.getMessage());
    }

    public void setMessage(String msg) {
        json.addProperty("message", msg);
    }

    @Override
    public void setStatusCode(StatusCode statusCode) {
        super.setStatusCode(statusCode);
        json.addProperty("code", statusCode.getCode());
        json.addProperty("message", statusCode.getMessage());
    }

    @Override
    public byte[] getBody() {
        setBody(gson.toJson(json));
        return super.getBody();
    }

    public JsonObject getJsonObject() {
        return json;
    }

    public void set(String key, JsonElement value) {
        json.add(key, value);
    }

    public void set(String key, Object value) {
        json.add(key, jsonParser.parse(gson.toJson(value)));
    }

    public void set(String key, String value) {
        json.addProperty(key, value);
    }

    public void set(String key, Number value) {
        json.addProperty(key, value);
    }

    public void set(String key, char value) {
        json.addProperty(key, value);
    }

    public void set(String key, boolean value) {
        json.addProperty(key, value);
    }
}
