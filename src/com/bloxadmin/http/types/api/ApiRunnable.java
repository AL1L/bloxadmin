package com.bloxadmin.http.types.api;

import com.bloxadmin.http.Request;

public interface ApiRunnable {

    void call(Request req, ApiResponse res);
}
